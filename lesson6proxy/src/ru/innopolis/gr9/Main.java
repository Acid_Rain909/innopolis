package ru.innopolis.gr9;

import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        int[] mass = {3, 3, 4, 6, 1, 2, 6, 3, 7, 9, 0, 10, 14, 18};
	    MathBox mb = new MathBox(mass);

        System.out.println(mb.getList());
        mb.listSumm();
        mb.divider(2);
        mb.listSumm();
        System.out.println(mb);

        ExMath realMB = new MathBox(mass);
        ExMath proxyMB = (ExMath) Proxy.newProxyInstance(
                MathBoxInvocationHandler.class.getClassLoader(),
                new Class[]{ExMath.class},
                new MathBoxInvocationHandler(realMB));
        proxyMB.getList();
        proxyMB.logText();
    }
}
