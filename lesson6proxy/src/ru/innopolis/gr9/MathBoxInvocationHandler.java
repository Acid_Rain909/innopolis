package ru.innopolis.gr9;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MathBoxInvocationHandler implements InvocationHandler{
        private ExMath mb;

        public MathBoxInvocationHandler(ExMath mb) {
            this.mb = mb;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Class clazz = mb.getClass();

            Method[] methods = clazz.getMethods();
            System.out.println(method.getName());
            System.out.println(method.isAnnotationPresent(ClearData.class));
            if(method.isAnnotationPresent(ClearData.class)){
                mb.clearHash();
                System.out.println("CLEARED HASH!!!");
                return null;
            }

            if (mb.getClass().getAnnotation(Logged.class)!=null){
                System.out.println("This is PROXY text!");
                Object result = method.invoke(mb, args);
                System.out.println(result);

                return result;
            } else {
                return method.invoke(mb, args);
            }
        }

}
