package ru.innopolis.gr9;

import java.util.HashSet;

public interface ExMath {
    @ClearData
    void logText();
    void listSumm();
    void divider(int div);
    HashSet<Integer> getList();
    void clearHash();

}
