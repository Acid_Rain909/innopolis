package ru.innopolis.stc9.lesson16;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    final static Logger logger = Logger.getLogger(Main.class);
    public static final String SOURCES_DEST = "E://testSet//simpletest"; //Путь к вашим файлам
    public static final String[] WORDS = {"CREQIZATERRATOMORF", "starter", "ffdf", "wfrrf", "cdcd", "dc"}; //Слова которые ищем
    public static final String RES = "D://searcherResult.txt"; //Куда пишем результат
    public static final SearchBase SEARCH_BASE = new SearchBase();

    public static void main(String[] args) {

        SEARCH_BASE.getOccurencies(sourcesToArr(SOURCES_DEST), WORDS, RES);

    }

    //???Test
    public static String[] sourcesToArr(String sourcesDest) {
        File dir = new File(sourcesDest);
        File[] arrFiles = dir.listFiles();
        int fileCount = arrFiles.length;

        String[] sources = new String[fileCount];
        for (int i = 0; i < fileCount; i++) {
            sources[i] = arrFiles[i].toString();
        }
        return sources;
    }
}