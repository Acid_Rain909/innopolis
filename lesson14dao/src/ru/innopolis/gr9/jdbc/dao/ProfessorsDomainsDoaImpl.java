package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManager;
import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManagerJDBCImpl;
import ru.innopolis.gr9.jdbc.pojo.ProfessorsDomains;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProfessorsDomainsDoaImpl implements ProfessorDomainsDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addDomain(ProfessorsDomains domain) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO professors_domains (id, professor_id, domain_name) " +
                    "values ( ?, ?, ?)");

            statement.setInt(1, domain.getId());
            statement.setInt(2, domain.getProfessorId());
            statement.setString(3, domain.getDomainName());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public boolean deleteDomainById(int id) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement("DELETE FROM professors_domains WHERE id = ?");

            statement.setInt(1, id);

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
