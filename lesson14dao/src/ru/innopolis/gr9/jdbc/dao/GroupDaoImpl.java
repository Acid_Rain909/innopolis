package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManager;
import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManagerJDBCImpl;
import ru.innopolis.gr9.jdbc.pojo.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GroupDaoImpl implements GroupDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addGroup(Group group) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO group (id, group_name) " +
                    "values ( ?, ?)");

            statement.setInt(1, group.getId());
            statement.setString(2, group.getGroupName());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public boolean deleteGroupById(int id) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement("DELETE FROM group WHERE id = ?");

            statement.setInt(1, id);

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
