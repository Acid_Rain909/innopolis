package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManager;
import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManagerJDBCImpl;
import ru.innopolis.gr9.jdbc.pojo.Shedule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SheduleDaoImpl implements SheduleDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addItemToShedule(Shedule item) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO shedule (id, group_id, domain_id) " +
                    "values ( ?, ?, ?)");

            statement.setInt(1, item.getId());
            statement.setInt(2, item.getGroupId());
            statement.setInt(3, item.getDomainId());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public boolean deleteSheduleItemById(int id) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement("DELETE FROM shedule WHERE id = ?");

            statement.setInt(1, id);

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
