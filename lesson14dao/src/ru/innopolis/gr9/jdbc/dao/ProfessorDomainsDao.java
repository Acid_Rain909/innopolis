package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.pojo.ProfessorsDomains;

public interface ProfessorDomainsDao {
    public boolean addDomain(ProfessorsDomains domain);
    public boolean deleteDomainById(int id);
}
