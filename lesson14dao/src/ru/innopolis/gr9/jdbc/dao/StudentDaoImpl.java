package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManager;
import ru.innopolis.gr9.jdbc.ConnectionManager.ConnectionManagerJDBCImpl;
import ru.innopolis.gr9.jdbc.pojo.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentDaoImpl implements StudentDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addStudent(Student student) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO student (id, name, group_id) " +
                    "values ( ?, ?, ?)");

            statement.setInt(1, student.getId());
            statement.setString(2, student.getName());
            statement.setInt(3, student.getgroupId());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public Student getStudentById(int id) {

        Connection connection = connectionManager.getConnection();
        Student student = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("SELECT * " +
                    "FROM student WHERE id = ?");

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                student = new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("group_id"));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public boolean updateStudent(Student student) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;


        try {
            statement = connection.prepareStatement("UPDATE student " +
                    "SET name = ?, group_id = ?" +
                    "WHERE id = ?");

            statement.setString(1, student.getName());
            statement.setInt(2, student.getgroupId());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return true;
    }

    @Override
    public boolean deleteStudentById(int id) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;


        try {
            statement = connection.prepareStatement("DELETE FROM student WHERE id = ?");

            statement.setInt(1, id);

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
