package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.pojo.Professor;

import java.sql.SQLException;

public interface ProfessorDao {
    public boolean addProfessor(Professor professor);
    public Professor getProfessorById(int id) throws SQLException;
    public boolean updateProfessor(Professor professor) throws SQLException;
    public boolean deleteProfessorById(int id);
}
