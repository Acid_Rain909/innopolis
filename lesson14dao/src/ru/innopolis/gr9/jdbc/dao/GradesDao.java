package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.pojo.Grades;

public interface GradesDao {
    public boolean addGrade(Grades grade);
}
