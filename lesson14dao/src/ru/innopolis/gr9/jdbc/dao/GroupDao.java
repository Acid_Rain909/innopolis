package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.pojo.Group;

import java.sql.SQLException;

public interface GroupDao {
    public boolean addGroup(Group group);
    public boolean deleteGroupById(int id);
}
