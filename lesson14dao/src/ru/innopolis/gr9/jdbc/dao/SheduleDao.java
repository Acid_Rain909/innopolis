package ru.innopolis.gr9.jdbc.dao;

import ru.innopolis.gr9.jdbc.pojo.Shedule;

public interface SheduleDao {
    public boolean addItemToShedule(Shedule shedule);
    public boolean deleteSheduleItemById(int id);
}
