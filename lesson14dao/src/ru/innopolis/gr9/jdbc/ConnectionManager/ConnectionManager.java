package ru.innopolis.gr9.jdbc.ConnectionManager;

import java.sql.Connection;

public interface ConnectionManager {
    public Connection getConnection();
}
