package ru.innopolis.gr9;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static ArrayList<Employee> employers = new ArrayList();
    private static String filename = "employers.txt";

    public static void main(String[] args) {
        Employee employee1 = new Employee();
        employee1.setName("Name1");
        employee1.setJob("Java Junior");
        employee1.setAge(22);
        employee1.setSalary(30000);

        Employee employee2 = new Employee();
        employee2.setName("Name2");
        employee2.setJob("Java Middle");
        employee2.setAge(34);
        employee2.setSalary(65000);

        Employee employee3 = new Employee();
        employee3.setName("Name3");
        employee3.setJob("Java Middle");
        employee3.setAge(31);
        employee3.setSalary(70000);

        Employee employee4 = new Employee();
        employee4.setName("Name4");
        employee4.setJob("Java Senior");
        employee4.setAge(38);
        employee4.setSalary(125000);

        Employee employee5 = new Employee();
        employee5.setName("Name5");
        employee5.setJob("Java Senior");
        employee5.setAge(43);
        employee5.setSalary(156000);

        System.out.println(save(employee1));
        System.out.println(save(employee2));
        System.out.println(save(employee3));
        System.out.println(save(employee4));
        System.out.println(save(employee5));

        System.out.println(getByName("Name3"));
        System.out.println(getByName("Name6"));

        System.out.println(delete(employee1));
        System.out.println(getByName("Name1"));

        System.out.println(getByJob("Java Middle"));

        System.out.println(changeAllWork("Java Senior", "Java Crazy Senior"));

        System.out.println(getByName("Name4"));
        System.out.println(getByName("Name5"));

        System.out.println(saveOrUpdate(employee2));
        System.out.println(saveOrUpdate(employee1));

    }

    public static boolean save(Employee employee) {
        employers.add(employee);
        OOS();
        return true;
    }

    public static boolean delete(Employee employee) {

        ArrayList<Employee> newEmployers = null;
        newEmployers = OIS(newEmployers);

        for (Employee emp : newEmployers) {
            if (emp.equals(employee)) {
                newEmployers.remove(emp);
                employers = newEmployers;
                OOS();
                return true;
            }
        }

        return false;
    }


    public static Employee getByName(String name) {
        ArrayList<Employee> newEmployers = null;
        newEmployers = OIS(newEmployers);

        for (Employee emp : newEmployers) {
            if (emp != null && emp.getName().equals(name)) return emp;
        }
        return null;
    }

    public static List<Employee> getByJob(String job) {
        List<Employee> list = new ArrayList<>();

        ArrayList<Employee> newEmployers = null;
        newEmployers = OIS(newEmployers);

        for (Employee emp : newEmployers) {
            if (emp.getJob().equals(job)) {
                list.add(emp);
            }
        }

        if (list.size() <= 0) {
            System.out.println("Sorry. No employers with this job...");
        }
        return list;
    }

    public static boolean saveOrUpdate(Employee employee) {
        if (getByName(employee.getName()) == null) {
            save(employee);
            return true;
        }
        return false;
    }

    public static boolean changeAllWork(String fromJob, String toJob) {
        ArrayList<Employee> newEmployers = null;
        newEmployers = OIS(newEmployers);

        for (Employee emp : newEmployers) {
            if (emp.getJob().equals(fromJob)) {
                emp.setJob(toJob);
            }
        }
        employers = newEmployers;
        OOS();
        return true;
    }


    public static ArrayList<Employee> OIS(ArrayList<Employee> newEmployers) {

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            newEmployers = (ArrayList<Employee>) ois.readObject();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return newEmployers;
    }

    public static void OOS() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(employers);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
