<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Логин/пароль</title>
</head>
<body>
<%=request.getAttribute("message") + "<br>"%>
<%
    Long currentTime = System.currentTimeMillis();
    String timeMessage = "Current Time: " + currentTime;
%>
<%=timeMessage%>
<%=("authErr".equals(request.getParameter("errorMsg"))) ? "Неверное имя пользователя/пароль" : ""%><BR>
<%=("noAuth".equals(request.getParameter("errorMsg"))) ? "Наверное, стоит сначала пройти авторизацию" : ""%><BR>
<form action="${pageContext.request.contextPath}/login" method="post">
    <input type="text" value="user" name="userName"><BR>
    <input type="text" value="password" name="userPassword"><BR>
    <input type="submit" value="OK">
</form>
</body>
</html>
