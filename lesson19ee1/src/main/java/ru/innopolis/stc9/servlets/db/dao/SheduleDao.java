package ru.innopolis.stc9.servlets.db.dao;

import ru.innopolis.stc9.servlets.pojo.Shedule;

public interface SheduleDao {
    public boolean addItemToShedule(Shedule shedule);
    public boolean deleteSheduleItemById(int id);
}
