package ru.innopolis.stc9.servlets.pojo;

public class ProfessorsDomains {
    private int id;
    private int professorId;
    private Professor professor;
    private String domainName;

    public ProfessorsDomains(int id, int professor_id, String domain_name) {
        this.id = id;
        this.professorId = professor_id;
        this.domainName = domain_name;
    }

    public ProfessorsDomains(int id, int professor_id, Professor professor, String domain_name) {
        this.id = id;
        this.professorId = professor_id;
        this.professor = professor;
        this.domainName = domain_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfessorId() {
        return professorId;
    }

    public void setProfessorId(int professor_id) {
        this.professorId = professor_id;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domain_name) {
        this.domainName = domain_name;
    }
}
