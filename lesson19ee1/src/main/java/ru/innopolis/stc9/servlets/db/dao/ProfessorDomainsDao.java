package ru.innopolis.stc9.servlets.db.dao;

import ru.innopolis.stc9.servlets.pojo.ProfessorsDomains;

public interface ProfessorDomainsDao {
    public boolean addDomain(ProfessorsDomains domain);
    public boolean deleteDomainById(int id);
}
