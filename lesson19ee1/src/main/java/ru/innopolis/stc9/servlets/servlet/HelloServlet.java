package ru.innopolis.stc9.servlets.servlet;

import ru.innopolis.stc9.servlets.pojo.Student;
import ru.innopolis.stc9.servlets.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class HelloServlet extends HttpServlet {
    private StudentService studentService = new StudentService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String studId = req.getParameter("stud_id");
        if (studId != null) {
            Student student = studentService.getStudentById(Integer.parseInt(studId));
            resp.getWriter().println(student.getName());
        } else {
            resp.getWriter().println("Nothing");
        }

        /*String formSent = req.getParameter("form_sent");
        Student student1 = new Student(3,"Vova", 45);

        if (formSent != null) {
            studentService.addStudent(student1);
        }*/
    }
}
