package ru.innopolis.stc9.servlets.db.dao;


import ru.innopolis.stc9.servlets.pojo.Grades;

public interface GradesDao {
    public boolean addGrade(Grades grade);
}
