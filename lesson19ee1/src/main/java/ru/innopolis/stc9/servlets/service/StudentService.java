package ru.innopolis.stc9.servlets.service;

import org.apache.log4j.Logger;
import ru.innopolis.stc9.servlets.db.dao.StudentDao;
import ru.innopolis.stc9.servlets.db.dao.StudentDaoImpl;
import ru.innopolis.stc9.servlets.pojo.Student;

import java.sql.SQLException;

public class StudentService {
    Logger logger = Logger.getLogger(StudentService.class);
    StudentDao studentDao = new StudentDaoImpl();

    public Student getStudentById(int id) {
        try {
            logger.info("Getting student by id = " + id);
            return studentDao.getStudentById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*public void addStudent(Student student) {
            studentDao.addStudent(student);
    }*/
}
