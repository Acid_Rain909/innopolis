package ru.innopolis.stc9.servlets.pojo;

public class Shedule {
    private int id;
    private int groupId;
    private Group group;
    private int domainId;
    private ProfessorsDomains domain;

    public Shedule(int id, int group_id, int domain_id) {
        this.id = id;
        this.groupId = group_id;
        this.domainId = domain_id;
    }

    public Shedule(int id, int group_id, Group group, int domain_id, ProfessorsDomains domain) {
        this.id = id;
        this.groupId = group_id;
        this.group = group;
        this.domainId = domain_id;
        this.domain = domain;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int group_id) {
        this.groupId = group_id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domain_id) {
        this.domainId = domain_id;
    }

    public ProfessorsDomains getDomain() {
        return domain;
    }

    public void setDomain(ProfessorsDomains domain) {
        this.domain = domain;
    }
}
