package ru.innopolis.stc9.servlets.pojo;

public class Student {
    private int id;
    private String name;
    private int groupId;

    public Student(int id, String name, int group_id) {
        this.id = id;
        this.name = name;
        this.groupId = group_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getgroupId() {
        return groupId;
    }

    public void setgroupId(int group_id) {
        this.groupId = group_id;
    }
}
