package ru.innopolis.stc9.servlets.pojo;

public class Group {
    private int id;
    private String groupName;

    public Group(int id, String group_name) {
        this.id = id;
        this.groupName = group_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String group_name) {
        this.groupName = group_name;
    }
}
