package ru.innopolis.stc9.servlets.db.dao;

import ru.innopolis.stc9.servlets.db.connection.ConnectionManager;
import ru.innopolis.stc9.servlets.db.connection.ConnectionManagerJDBCImpl;
import ru.innopolis.stc9.servlets.pojo.Professor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProfessorDaoImpl implements ProfessorDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addProfessor(Professor professor) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO professor (id, name, group_id) " +
                    "values ( ?, ?, ?)");

            statement.setInt(1, professor.getId());
            statement.setString(2, professor.getName());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public Professor getProfessorById(int id) {

        Connection connection = connectionManager.getConnection();
        Professor professor = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("SELECT * " +
                    "FROM professor WHERE id = ?");

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                professor = new Professor(
                        resultSet.getInt("id"),
                        resultSet.getString("name"));

            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return professor;
    }

    @Override
    public boolean updateProfessor(Professor professor) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;


        try {
            statement = connection.prepareStatement("UPDATE professor " +
                    "SET name = ? WHERE id = ?");

            statement.setString(1, professor.getName());

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return true;
    }

    @Override
    public boolean deleteProfessorById(int id) {

        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = null;


        try {
            statement = connection.prepareStatement("DELETE FROM professor WHERE id = ?");

            statement.setInt(1, id);

            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
