package ru.innopolis.stc9.servlets.db.dao;


import ru.innopolis.stc9.servlets.db.connection.ConnectionManager;
import ru.innopolis.stc9.servlets.db.connection.ConnectionManagerJDBCImpl;
import ru.innopolis.stc9.servlets.pojo.Grades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GradesDaoImpl implements GradesDao {
    private static ConnectionManager connectionManager = ConnectionManagerJDBCImpl.getInstance();

    @Override
    public boolean addGrade(Grades grade) {
        Connection connection = connectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO grades (id, professor_id, student_id, domain_id, date, rating) " +
                    "values ( ?, ?, ?, ?, ?, ?)");

            statement.setInt(1, grade.getId());
            statement.setInt(2, grade.getProfessorId());
            statement.setInt(3, grade.getStudentId());
            statement.setInt(4, grade.getDomainId());
            statement.setDate(5, grade.getDate());
            statement.setInt(6, grade.getRating());


            statement.executeQuery();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
