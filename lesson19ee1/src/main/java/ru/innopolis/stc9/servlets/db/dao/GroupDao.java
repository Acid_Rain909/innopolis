package ru.innopolis.stc9.servlets.db.dao;

import ru.innopolis.stc9.servlets.pojo.Group;

public interface GroupDao {
    public boolean addGroup(Group group);
    public boolean deleteGroupById(int id);
}
