package ru.innopolis.stc9.servlets.pojo;

import java.sql.Date;

public class Grades {
    private int id;
    private int professorId;
    private Professor professor;
    private int studentId;
    private Student student;
    private int domainId;
    private ProfessorsDomains domain;
    private Date date;
    private int rating;

    public Grades(int id, int professorId, int studentId, int domainId, Date date, int rating) {
        this.id = id;
        this.professorId = professorId;
        this.studentId = studentId;
        this.domainId = domainId;
        this.date = date;
        this.rating = rating;
    }

    public Grades(int id, int professorId, Professor professor, int studentId, Student student, int domainId, ProfessorsDomains domain, Date date, int rating) {
        this.id = id;
        this.professorId = professorId;
        this.professor = professor;
        this.studentId = studentId;
        this.student = student;
        this.domainId = domainId;
        this.domain = domain;
        this.date = date;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfessorId() {
        return professorId;
    }

    public void setProfessorId(int professorId) {
        this.professorId = professorId;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    public ProfessorsDomains getDomain() {
        return domain;
    }

    public void setDomain(ProfessorsDomains domain) {
        this.domain = domain;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
