package ru.innopolis.stc9.servlets.db.dao;

import ru.innopolis.stc9.servlets.pojo.Student;

import java.sql.SQLException;

public interface StudentDao {
    public boolean addStudent(Student student);
    public Student getStudentById(int id) throws SQLException;
    public boolean updateStudent(Student student) throws SQLException;
    public boolean deleteStudentById(int id);
}
