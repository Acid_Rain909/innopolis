package ru.innopolis.stc9.servlets.db.dao;


import ru.innopolis.stc9.servlets.pojo.Professor;

import java.sql.SQLException;

public interface ProfessorDao {
    public boolean addProfessor(Professor professor);
    public Professor getProfessorById(int id) throws SQLException;
    public boolean updateProfessor(Professor professor) throws SQLException;
    public boolean deleteProfessorById(int id);
}
