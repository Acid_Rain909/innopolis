package ru.innopolis.gr9;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Timer timer = new Timer();
        Interval interval1 = new Interval(timer, 1);
        Interval interval2 = new Interval(timer, 5);
        Interval interval3 = new Interval(timer, 7);
        new Thread(interval1).start();
        new Thread(interval2).start();
        new Thread(interval3).start();

        timer.startTimer(interval1, interval2, interval3);
    }
}

class Timer {
    public int startTime = 0;

    public void startTimer(Interval i1, Interval i2, Interval i3) {
        while (true) {
            synchronized (this) {
                startTime += 1;
                System.out.println(startTime);
                this.notifyAll();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}

class Interval implements Runnable {

    Timer timer;
    int interval;

    Interval(Timer timer, int interval) {
        this.timer = timer;
        this.interval = interval;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (timer) {
                try {
                    while (true) {
                        timer.wait();
                        if (timer.startTime % this.interval == 0 && this.interval != 1) {
                            System.out.println("Interval " + this.interval);
                        }
                    }
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
