package ru.innopolis.gr9;

import java.util.HashSet;
import java.util.Iterator;

public class MathBox {
    HashSet<Integer> list = new HashSet<>();

    MathBox(int [] mass){
        for (int el:
             mass) {
            list.add(el);
        }

        System.out.println("Duplicate elements was cut!");
    }

    public void listSumm() {
        int summ = 0;
        for (int el:
             list) {
            summ += el;
        }

        System.out.println("Summ = " + summ);
    }

    public void divider(int div) {
		if(div == 0) return;
        int forDivide;
        HashSet<Integer> dividedList = new HashSet<>();
        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()){
            forDivide = iterator.next();
            forDivide = forDivide/div;
            dividedList.add(forDivide);
        }

        list = dividedList;

        System.out.println("Divided!");
    }

    public HashSet<Integer> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "MathBox{" +
                "list=" + list +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MathBox mathBox = (MathBox) o;

        return list != null ? list.equals(mathBox.list) : mathBox.list == null;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }
}
